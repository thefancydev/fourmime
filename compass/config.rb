http_path    = "/"
project_path = "."
css_dir      = "public/assets/CSS"
sass_dir     = "source/assets/SCSS"
images_dir   = "public/assets/images"

# when using SCSS:
sass_options = {
  :syntax => :scss
}
